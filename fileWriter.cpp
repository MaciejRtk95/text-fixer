bool writeToFile(std::string fileName, std::string& stringToWrite){
    std::ofstream file;
    file.open(fileName, std::ios::trunc);

    if (file.is_open()) {
        file << stringToWrite;
        file.close();
        return 0;
    }
    else {
        file.close();
        return 1;
    }
}
