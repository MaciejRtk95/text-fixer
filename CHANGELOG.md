# Text Fixer changelog

Every merge to master indicates a new version

You want to report a bug or want to suggest a feature? Open an issue at https://www.gitlab.com/matsaki95/clogop



### Alpha 4 "Practise safe hex"

No more random nulls and who knows what else in the output files, the main feature of this update is that the program won't read further than it should (outside of the string)

- When reading the input to write the corrected output, safety measures are in place that prevent the program from reading outside the input

- Numbers with dots and commas (for example 9,000, 3.14) no longer get screwed up by getting a space after the dot/comma.



### Alpha 3 "Compatibility"

The program now can leave code and links unharmed from the correction process. Specifically:

- Inspired from MarkDown, you can now put your code or whatever you want to not be changed inside backquotes. For example, this is marked as code and won't get messed up: \`std::cout << "Hello world" << std::endl\`

- The program can now detect links and not destroy them. They will have to fall in one of the following formats:
    - The link starts with `http`. For example: `https://www.gitlab.com/matsaki95/text-fixer`.
    - The link starts with `www`. For example: `www.gitlab.com/matsaki95/text-fixer`
    - The link doesn't have anything before the domain name and uses `.com`. For example `gitlab.com/matsaki95/text-fixer`

- On the corrections side, spaces before a new paragraph get deleted



### Alpha 2b "Humans forget"

Some features were forgotten to be included in Alpha 2... This is the rest of them:

- Only one `:` is allowed at the time

- Double quotes! They don't get all messed up anymore, instead the program recognises the opening and the closing quote and moves them accordingly



### Alpha 2 "I stand corrected"

A lot of new corrections in this update. Specifically:

- The program now positions properly periods, commas, question marks, semicolons/Greek question marks, exclamation marks, colons/double dots/`:`s, parenthesis and forward slashes. Most of these characters are also filtered so that there's only one at the time (not all, periods for example get set to three if there are two or more).

- `\` now gets replaced with `/` and of course gets positioned properly

- No more spaces at the ends of paragraphs



### Alpha 1 "Space files"

The basic core of the program: reading a file, going through what was read, fixing mistakes and finally writing the corrected version to a file (whether that is the same or a different one, it doesn't matter). Specifically, the new stuff is:

- The program now has file reading and writing capabilities. It will read from `input.txt`, which has to be in its running directory and it will write to `output.txt`, in the same directory. If a file called `output.txt` already exists, it will be overwritten.

- No more annoying double  spaces that just look weird and mess with your OCD. Only one space at a time is allowed!



### Alpha 0b "Choose a license, any license"

Added a license. Specifically, the Apache License 2.0.



### Alpha 0 "Quitter"

The program runs and finishes. Does nothing. Just to initialise the repository.