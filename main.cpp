#include <iostream>
#include "fileIO.h"
#include "stringProcessor.cpp"

int main() {
    std::string stringToModify;

    if (readFile("input.txt", stringToModify) == 1) {//Think true, there was an error
        std::cout << "Error reading file" << std::endl;
        return 1;
    }

    std::string modifiedString = processString(stringToModify);

    if (writeToFile("output.txt", modifiedString) == 1) {
        std::cout << "Error writing to file" << std::endl;
        return 2;
    }

    return 0;
}
