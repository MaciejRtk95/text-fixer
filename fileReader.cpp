bool readFile(std::string fileName, std::string& stringToReadTo){
    std::ifstream file;
    file.open(fileName);
    stringToReadTo = "";
    std::string buffer;

    if (file.is_open()) {
        while (!file.eof()) {
            getline(file, buffer);
            stringToReadTo += buffer;
            stringToReadTo += '\n';
        }
        file.close();
        stringToReadTo.pop_back();//This is to delete the last character since it's purpose is to separate lines and there is no other line at the back
        return 0;//Everything run fine
    }
    else {
        file.close();
        return 1;//Error reading the file
    }
}
