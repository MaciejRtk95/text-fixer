std::string processString(std::string& input) {
    std::string output = "";
    bool insideDoubleQuotes = false;
    bool insideCode = false;

    for (int i = 0; i < input.length(); i++) {
        if (insideCode) {
            output += input[i];
            if (input[i] == '`')
                insideCode = false;
        }
        else {
            switch (input[i]) {
                case ' ':
                    output += ' ';
                    while (input[i + 1] == ' ' && i + 1 < input.length())
                        i++;//Skips any extra, unneeded spaces. Remember that the for loop also iterates `i`.
                    break;

                case '.':
                    if (output[output.length() - 1] == ' ' && output.length() > 0)//We don't want a space before a dot/comma/whatever
                        output.pop_back();
                    output += '.';
                    if (input[i + 1] == '.' && i + 1 < input.length()) {//We aim for `...`, no more no less
                        output += "..";
                        while (input[i + 1] == '.' && i + 1 < input.length())
                            i++;//We skip whatever other dot
                    }
                    if (input[i + 1] != ' ' && i + 1 < input.length())//If i + 1 is not a space and an actual character in input
                        if (input[i + 1] < '0' || input[i + 1] > '9')//And if i + 1 is not a number
                            output += ' ';//Then a space is needed to be put
                    break;

                case ',':
                    if (output[output.length() - 1] == ' ' && output.length() > 0)
                        output.pop_back();
                    output += ',';
                    while (input[i + 1] == ',' && i + 1 < input.length())
                        i++;//Skips extra, unneeded commas. Yes, extra commas is a thing some people do...
                    if (input[i + 1] != ' ' && i + 1 < input.length())
                        if (input[i + 1] < '0' || input[i + 1] > '9')
                            output += ' ';
                    break;

                case '?':
                    if (output[output.length() - 1] == ' ' && output.length() > 0)
                        output.pop_back();
                    output += '?';
                    while (input[i + 1] == '?' && i + 1 < input.length())
                        i++;//This makes sense, right????
                    if (input[i + 1] != ' ' && i + 1 < input.length())
                        output += ' ';
                    break;

                case ';':
                    if (output[output.length() - 1] == ' ' && output.length() > 0)
                        output.pop_back();
                    output += ';';
                    while (input[i + 1] == ';' && i + 1 < input.length())
                        i++;
                    if (input[i + 1] != ' ' && i + 1 < input.length())
                        output += ' ';
                    break;

                case '!':
                    if (output[output.length() - 1] == ' ' && output.length() > 0)
                        output.pop_back();
                    output += '!';
                    while (input[i + 1] == '!' && i + 1 < input.length())
                        i++;
                    if (input[i + 1] != ' ' && i + 1 < input.length())
                        output += ' ';
                    break;

                case ':':
                    if (output[output.length() - 1] == ' ' && output.length() > 0)
                        output.pop_back();
                    output += ':';
                    while (input[i + 1] == ':' && i + 1 < input.length())
                        i++;
                    if (input[i + 1] != ' ' && i + 1 < input.length())
                        output += ' ';
                    break;

                case '\"':
                    if (!insideDoubleQuotes) {//If not inside double quotes, this will be the opening one
                        if (output[output.length() - 1] != ' ' && output.length() > 0)
                            if (output[output.length() - 1] != '\n')
                                if (output[output.length() - 1] != '/')
                                    output += ' ';
                        output += '\"';
                        while (input[i + 1] == ' ' && i + 1 < input.length())
                            i++;
                    }
                    else {
                        if (output[output.length() - 1] == ' ' && output.length() > 0)
                            output.pop_back();
                        output += '\"';
                        if (input[i + 1] != ' ' && i + 1 < input.length())
                            output += ' ';
                    }
                    insideDoubleQuotes = !insideDoubleQuotes;
                    break;

                case '`':
                    output += '`';
                    insideCode = true;
                    break;

                case '(':
                    if (output[output.length() - 1] != ' ' && output.length() > 0)
                        output += ' ';
                    output += '(';
                    while (input[i + 1] == ' ' && i + 1 < input.length())
                        i++;
                    break;

                case ')':
                    if (output[output.length() - 1] == ' ' && output.length() > 0)
                        output.pop_back();
                    output += ") ";
                    while (input[i + 1] == ' ' && i + 1 < input.length())
                        i++;
                    break;

                case '/':
                    if (output[output.length() - 1] == ' ' && output.length() > 0)
                        output.pop_back();
                    output += '/';
                    while (input[i + 1] == '/' && i + 1 < input.length())
                        i++;
                    while (input[i + 1] == ' ' && i + 1 < input.length())
                        i++;
                    break;

                case '\\':
                    if (output[output.length() - 1] == ' ' && output.length() > 0)
                        output.pop_back();
                    output += '/';
                    while (input[i + 1] == '\\' && i + 1 < input.length())
                        i++;
                    while (input[i + 1] == ' ' && i + 1 < input.length())
                        i++;
                    break;

                case '\n':
                    if (output[output.length() - 1] == ' ' && output.length() > 0)
                        output.pop_back();
                    output += '\n';
                    while (input[i + 1] == ' ' && i + 1 < input.length())
                        i++;
                    break;

                case 'h':
                    output += 'h';
                    if (input[i + 1] == 't' && input[i + 2] == 't' && input[i + 3] == 'p' && i + 3 < input.length()) {//if it starts as http then it and the rest is probably a link
                        while (input[i + 1] != ' ' && i + 1 < input.length()) {
                            output += input[i + 1];
                            i++;//We don't want to modify the link, so we just import it as is until it's finished
                        }
                    }
                    break;

                case 'w':
                    output += 'w';
                    if (input[i + 1] == 'w' && input[i + 2] == 'w' && i + 2 < input.length()) {
                        output += 'w';
                        while (input[i + 1] != ' ' && i + 1 < input.length()) {
                            output += input[i + 1];
                            i++;
                        }
                    }
                    break;

                case 'c':
                    //We won't add c right from the start because we might use pop_back()
                    if (input[i - 1] == '.' && input[i + 1] == 'o' && input[i + 2] == 'm' && i - 1 >= 0 && i + 2 < input.length()) {
                        output.pop_back();//The dot would case would insert a space automatically, we'll have to remove it
                        output += 'c';
                        while (input[i + 1] != ' ' && i + 1 < input.length()) {
                            output += input[i + 1];
                            i++;
                        }
                    }
                    else
                        output += 'c';
                    break;

                default:
                    output += input[i];
                    break;
            }
        }
    }

    return output;
}
